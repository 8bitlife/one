package another

import (
	"fmt"

	"gitlab.com/8bitlife/one/some"
	"gitlab.com/8bitlife/one/v2/internal/fortest"
)

func init() {
	fmt.Println(some.Var, fortest.Parameter)
}
